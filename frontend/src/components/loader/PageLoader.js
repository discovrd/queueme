import React from "react";
import LoopIcon from '@material-ui/icons/Loop';

const PageLoader = () => (
    <div className="App">
        <LoopIcon />
        <div>loading...</div>
    </div>
);

export default PageLoader;